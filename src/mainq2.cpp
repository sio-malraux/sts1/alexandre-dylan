#include <libpq-fe.h>
#include <iostream>

using namespace std;

int main()
{
  int code_retour = 0;
  const char infos_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.moreau user=a.moreau";
  PGPing code_retour_de_ping;
  code_retour_de_ping = PQping(infos_connexion);

  if(code_retour_de_ping == PQPING_OK)
  {
    PGconn *connexion = PQconnectdb(infos_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de données  a été établie " << endl;
    }
    else
    {
      cerr << "Probleme de ping !" << endl;
    }
  }

  return code_retour;
}
