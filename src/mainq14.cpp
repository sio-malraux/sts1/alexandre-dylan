#include <libpq-fe.h>
#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

int main()
{
  int code_retour = 0;
  int PQprotocolVersion(const PGconn * conn);
  int PQserverVersion(const PGconn * conn);
  ExecStatusType etat_resultat;
  const char infos_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.moreau user=a.moreau";
  PGPing code_retour_de_ping;
  code_retour_de_ping = PQping(infos_connexion);

  if(code_retour_de_ping == PQPING_OK)
  {
    PGconn *connexion = PQconnectdb(infos_connexion);
    const char *encodage = PQparameterStatus(connexion, "server_encoding");
    char *reponse_requete;
    PGresult *requete = PQexec(connexion, "SET SCHEMA 'si6' ;SELECT  \"Animal\".nom, \"Animal\".sexe, \"Animal\".date_naissance, \"Animal\".commentaires, \"Race\".nom, \"Race\".description   FROM \"Animal\" inner join \"Race\" on \"Animal\".race_id =\"Race\".id WHERE \"Race\".nom ='Singapura' AND \"Animal\".sexe='Femelle' ");

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec " << endl;
      cout << "    les paramètres suivants : " << endl;
      cout << " * utilisateur : " << PQuser(connexion) << endl;
      cout << " * mot de passe : ";
      char *longueurmdp = PQpass(connexion);
      etat_resultat = PQresultStatus(requete);

      for(int i = 0; i < strlen(longueurmdp); i++)
      {
        cout << "*";
      }

      cout << endl << " * base de données : " << PQdb(connexion) << endl;
      cout << " * port TCP : " << PQport(connexion) << endl;
      cout << " * chiffrement SSL : ";
      int SSL = PQsslInUse(connexion);

      if(SSL == 1)
      {
        cout << "TRUE" << endl;
      }

      cout << " * encodage : " << encodage << endl;
      cout << " * version du protocole : " << PQprotocolVersion(connexion) << endl;
      cout << " * version du serveur : " << PQserverVersion(connexion) << endl;
      cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << endl;
      int nbLignes, nbColonnes, l, c;
      char *nom_colonne;
      nbLignes = PQntuples(requete);
      nbColonnes = PQnfields(requete);
      cout << "Nombre de lignes: " << nbLignes + 1 << endl;
      cout << "Nombre de colonnes: " << nbColonnes + 1 << endl;
      cout << "Résultat: " << endl;
      unsigned int colonne = PQnfields(requete);
      int longueurTitre = 0;

      for(c = 0; c < nbColonnes; c++)
      {
        nom_colonne = PQfname(requete, c);

        if(longueurTitre < strlen(nom_colonne))
        {
          longueurTitre = strlen(nom_colonne);
        }
      }

      for(int compteur = 0; compteur <= (1 + nbColonnes)*longueurTitre; compteur++)
      {
        cout << "-";
      }

      cout << "\n";

      for(c = 0; c < nbColonnes; c++)
      {
        nom_colonne = PQfname(requete, c);
        cout << left << setw(longueurTitre) << nom_colonne << " | " << flush;
      }

      cout << "\n";

      for(int compteur = 0; compteur <= longueurTitre * (nbColonnes + 1); compteur++)
      {
        cout << "-";
      }

      cout << "\n";

      for(l = 0; l < nbLignes; l++)
      {
        for(c = 0; c < nbColonnes; c++)
        {
          reponse_requete = PQgetvalue(requete, l, c);

          if(strlen(reponse_requete) <= longueurTitre)
          {
            cout << left << setw(longueurTitre) << reponse_requete << " | " << flush;
          }
          else
          {
            char *chaine = reponse_requete;
            chaine[longueurTitre] = '\0';
            chaine[longueurTitre - 1] = '.';
            chaine[longueurTitre - 2] = '.';
            chaine[longueurTitre - 3] = '.';
            cout << left << setw(longueurTitre) << chaine << " | " << flush;
          }
        }

        cout << "\n";

        for(int compteur = 0; compteur <= longueurTitre * (nbColonnes + 1); compteur++)
        {
          cout << "-";
        }

        cout << "\n" << endl;
      }
    }
  }
  else if(code_retour_de_ping == PQPING_REJECT)
  {
    cerr << "Le serveur est en cours d'exécution mais est dans un état qui interdit les connexions (démarrage, arrêt, restauration après crash)." << endl;
    code_retour = 1;
  }
  else if(code_retour_de_ping == PQPING_NO_RESPONSE)
  {
    cerr << "Le serveur n'a pas pu être contacté. Cela pourrait indiquer que le serveur n'est pas en cours d'exécution ou qu'il y a un problème avec les paramètres de connexion donnés (par exemple un mauvais numéro de port). Cela peut aussi indiquer un problème de connexion réseau (par exemple un pare-feu qui bloque la demande de connexion)." << endl;
    code_retour = 1;
  }
  else if(code_retour_de_ping == PQPING_NO_ATTEMPT)
  {
    cerr << "Aucune tentative n'a été faite pour contacter le serveur à cause des paramètres fournis erronnés ou à cause d'un problème au niveau client (par exemple un manque mémoire)." << endl;
    code_retour = 1;
  }

  if(etat_resultat == PGRES_EMPTY_QUERY)
  {
    cerr << "La chaîne envoyée au serveur était vide. " << endl;
  }
  else if(etat_resultat == PGRES_COMMAND_OK)
  {
    cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << endl;
  }
  else if(etat_resultat == PGRES_TUPLES_OK)
  {
    cout << "Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW)." << endl;
  }
  else if(etat_resultat == PGRES_COPY_OUT)
  {
    cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << endl;
  }
  else if(etat_resultat == PGRES_COPY_IN)
  {
    cout << "Début de la réception (sur le serveur) d'un flux de données." << endl;
  }
  else if(etat_resultat == PGRES_BAD_RESPONSE)
  {
    cerr << "La réponse du serveur n'a pas été comprise." << endl;
  }
  else if(etat_resultat == PGRES_NONFATAL_ERROR)
  {
    cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << endl;
  }
  else if(etat_resultat == PGRES_FATAL_ERROR)
  {
    cerr << "Une erreur fatale est survenue." << endl;
  }
  else if(etat_resultat == PGRES_COPY_BOTH)
  {
    cout << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, so this status should not occur in ordinary applications" << endl;
  }
  else if(etat_resultat == PGRES_SINGLE_TUPLE)
  {
    cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode simple ligne a été sélectionné pour cette requête" << endl;
  }

  return code_retour;
}
