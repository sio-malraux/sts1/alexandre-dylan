#include <libpq-fe.h>
#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

int main()
{
  int code_retour = 0;
  const char infos_connexion[] = "host=postgresql.bts-malraux72.net port=5432 dbname=a.moreau user=a.moreau";
  PGPing code_retour_de_ping;
  code_retour_de_ping = PQping(infos_connexion);

  if(code_retour_de_ping == PQPING_OK)
  {
    PGconn *connexion = PQconnectdb(infos_connexion);
    const char *encodage = PQparameterStatus(connexion, "server_encoding");

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec " << endl;
      cout << "    les paramètres suivants : " << endl;
      cout << " * utilisateur : " << PQuser(connexion) << endl;
      cout << " * mot de passe : ";
      char *longueurmdp = PQpass(connexion);

      for(int i = 0; i < strlen(longueurmdp); i++)
      {
        cout << "*";
      }

      cout << endl << " * base de données : " << PQdb(connexion) << endl;
      cout << " * port TCP : " << PQport(connexion) << endl;
      cout << " * chiffrement SSL : ";
      int SSL = PQsslInUse(connexion);

      if(SSL == 1)
      {
 cout << "TRUE" << endl;
      }

      cout << " * encodage : " << encodage << endl;
      cout << " * version du protocole : " << PQprotocolVersion(connexion) << endl;
      cout << " * version du serveur : " << PQserverVersion(connexion) << endl;
      cout << " * version de la bibliothèque 'libpq' du client : " << PQlibVersion() << endl;
    }
    else
    {
    cout << "En cours" << endl;
    }
  }

  return code_retour;
}
