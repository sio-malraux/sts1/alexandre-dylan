#include <libpq-fe.h>
#include <iostream>

using namespace std;

int main()
{
  const char infos_connexion[] = "host=postgresql.bts-malraux72.net";
  PGPing code_retour_de_ping;
  code_retour_de_ping = PQping(infos_connexion);

  if(code_retour_de_ping == PQPING_OK)
  {
    cout << "Ping ok" << endl;
  }
  else
  {
    cerr << "Probleme de ping !" << endl;
  }

  return 0;
}

